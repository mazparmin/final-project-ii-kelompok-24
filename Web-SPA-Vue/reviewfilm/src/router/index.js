import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
//import Films from '../views/Films.vue'
//import Film from '../views/Film.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/films',
    name: 'Films',
    component: () => import(/* webpackChunkName: "about" */ '../views/Films.vue')
  },
  {
    path: '/film/:id',
    name: 'Film',
    component: () => import(/* webpackChunkName: "about" */ '../views/Film.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
