
## Kelompok 24
# REVIEW FILM
### Nama Anggota:
1. Moch Fidri Lazuardi Mahendra
2. M. Miftah Hasan
3. Suparmin
Tema: Review Film

### Diagram ER :
![DIagram ER](https://gitlab.com/mazparmin/review-film/-/blob/master/ERD.png)
  
  ### Link Dokumentasi Postman: https://documenter.getpostman.com/view/18396045/UVR7LURZ

  
### Link Video:
<iframe width="560" height="315" src="https://www.youtube.com/embed/oUiLU9STzNk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


