<div>
    <h2>Edit Profil</h2>
        <form action="/profil/{{$profil->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" value="{{$profil->umur}}" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" value="{{$profil->alamat}}" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea type="text" class="form-control" name="bio" id="bio">{{$profil->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>