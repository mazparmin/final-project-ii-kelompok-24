<div>
    <h2>Edit Pengguna</h2>
        <form action="/user/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="username" id="username" value="{{$user->username}}" placeholder="Masukkan Username">
                @error('username')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Email</label>
                <textarea type="text" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="Masukkan Email"></textarea>
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" id="password" value="{{$user->password}}" placeholder="Masukkan Password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>