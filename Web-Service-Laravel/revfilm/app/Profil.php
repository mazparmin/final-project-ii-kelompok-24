<?php

namespace App;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $fillable = ['umur','bio','alamat', 'nama'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing ='false';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    
    public function users()
    {
        return $this->hasMany('App\User');
     
    }
}
