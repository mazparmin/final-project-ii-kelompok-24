<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = "peran";
    protected $fillable = ['peran', 'film_id', 'cast_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing ='false';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    
    public function cast()
    {
        return $this->belongsTo('App\Cast');
    } 

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
