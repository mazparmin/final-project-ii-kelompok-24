<?php
//use App\Profil;
//use Tymon\JWTAuth\Contracts\JWTSubject;
//use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;




namespace App;
use App\Profil;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    
    protected $table = "users";
    protected $fillable = [
        'username', 'email', 'password' , 'profil_id',  'email_verified_at'
    ];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

           // $model->profil_id = profil()->id;
            
        });
    }

    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }
}
