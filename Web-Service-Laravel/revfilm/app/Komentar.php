<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $fillable = ['kritik', 'rating', 'film_id', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing ='false';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    } 

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
