<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CastController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        $cast = Cast::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Cast berhasil ditampilkan',
            'data'    => $cast
        ]);
    }


    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $cast = Cast::create([
            'nama' =>  $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        if($cast){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Cast berhasil dibuat',
                'data'      =>  $cast
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Cast gagal dibuat'
        ], 409);
    }


    public function show($id)
    {
        $cast = Cast::find($id);

        if($cast)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Cast berhasil ditampilkan',
                'data'    => $cast
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }


    public function edit($id)
    {
   
    }


    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $cast = Cast::find($id);

        if($cast)
        {
             $cast->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Cast dengan nama : ' . $cast->nama . '  berhasil diupdate',
                'data' =>    $cast
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);

        if ($cast) {
            $cast->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data cast berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Cast dengan id : ' .  $id . '  tidak ditemukan',
        ], 404); 
    }
}
