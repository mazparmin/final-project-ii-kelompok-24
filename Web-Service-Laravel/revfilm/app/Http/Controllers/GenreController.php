<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GenreController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        $genre = Genre::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar Genre berhasil ditampilkan',
            'data'    => $genre
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $genre = Genre::create([
            'nama' => $request->nama
        ]);

        //success save to database
        if($genre) {

            return response()->json([
                'success' => true,
                'message' => 'Data Genre berhasil dibuat',
                'data'    => $genre  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Genre gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find genre by ID
        $genre = Genre::find($id);

        if($genre){
            return response()->json([
                'success' => true,
                'message' => 'Data Genre berhasil ditampilkan',
                'data'    => $genre 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Genre dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Genre by ID
        $genre = Genre::find($id);

        if($genre) {

            //update genre
            $genre->update([
                'nama' => $request->nama
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Genre diupdate',
                'data'    => $genre  
            ], 200);
        }

        //data genre not found
        return response()->json([
            'success' => false,
            'message' => 'Data Genre tidak ditemukan',
        ], 404);//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find genre by ID
        $genre = Genre::find($id);

        if($genre) {

            //delete Genre
            $genre->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Genre berhasil dihapus',
            ], 200);

        }

        //data Genre not found
        return response()->json([
            'success' => false,
            'message' => 'Genre Not Found',
        ], 404);
    }
}
