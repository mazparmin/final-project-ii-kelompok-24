<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Profil;

class ProfilController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        $profil = Profil::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Profil berhasil ditampilkan',
            'data'    => $profil
        ]);
    }

    
    public function create()
    {
       //
    }

    
    public function store(Request $request)
    {
                $allRequest = $request->all();
        
                $validator = Validator::make($allRequest , [
                    'nama' => 'required',
                    'umur' => 'required',
                    'bio' => 'required'
                ]);
        
                if($validator->fails()){
                    return response()->json($validator->errors() , 400);
                }
        
                $profil = Profil::create([
                    'nama' =>  $request->nama,
                    'umur' => $request->umur,
                    'bio' => $request->bio,
                    'alamat' => $request->alamat
                ]);
        
                if($profil){
                    return response()->json([
                        'success'   => true,
                        'message'   => 'Data Cast berhasil dibuat',
                        'data'      =>  $profil
                    ], 200);
                }
        
                return response()->json([
                    'success'   => false,
                    'message'   => 'Data Profil gagal dibuat'
                ], 409);
    }

    
    public function show($id)
    {
        $profil = Profil::find($id);

        if($profil)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Profil berhasil ditampilkan',
                'data'    => $profil
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    
    public function edit($id)
    {
    }

    
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profil = Profil::find($id);

        if($profil)
        {
             $profil->update([
                'nama' =>  $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
                'alamat' => $request->alamat
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Profil dengan nama : ' . $profil->nama . '  berhasil diupdate',
                'data' =>    $profil
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    
    public function destroy($id)
    {
        $profil = Profil::find($id);

        if ($profil) {
            $profil->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Profil berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Profil dengan id : ' .  $id . '  tidak ditemukan',
        ], 404); 
    }
}
