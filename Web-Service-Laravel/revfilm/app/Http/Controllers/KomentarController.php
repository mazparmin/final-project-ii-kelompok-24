<?php

namespace App\Http\Controllers;

use App\Komentar;
use Illuminate\Http\Request;

class KomentarController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        //get data from table komentar
        $komentar = Komentar::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar Komentar berhasil ditampilkan',
            'data'    => $komentar  
        ], 200);
    }

    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'kritik'  => 'required',
            'rating'  => 'required',
            'film_id' => 'required',
            'user_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $komentar = Komentar::create([
            'kritik'   => $request->kritik,
            'rating'   => $request->rating,
            'film_id'  => $request->film_id,
            'user_id'  => $request->user_id,
        ]);

        //success save to database
        if($komentar) {

            return response()->json([
                'success' => true,
                'message' => 'Data Komentar berhasil dibuat',
                'data'    => $komentar  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Komentar gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find komentar by ID
        $komentar = Komentar::find($id);

        //make response JSON
        if($komentar){
            return response()->json([
                'success' => true,
                'message' => 'Data Komentar berhasil ditampilkan',
                'data'    => $komentar 
            ], 200);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Data Komentar dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'kritik'  => 'required',
            'rating'  => 'required',
            'film_id' => 'required',
            'user_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Komentar by ID
        $komentar = Komentar::find($id);

        if($komentar) {

            //update Komentar
            $komentar->update([
                'kritik'   => $request->kritik,
            'rating'   => $request->rating,
            'film_id'  => $request->film_id,
            'user_id'  => $request->user_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar diupdate',
                'data'    => $komentar  
            ], 200);

        }

        //Komentar not found
        return response()->json([
            'success' => false,
            'message' => 'Komentar tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find komentar by ID
        $komentar = Komentar::find($id);

        if($komentar) {

            //delete komentar
            $komentar->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Komentar berhasil dihapus',
            ], 200);

        }

        //Komentar not found
        return response()->json([
            'success' => false,
            'message' => 'Komentar Not Found',
        ], 404);
    }
}
