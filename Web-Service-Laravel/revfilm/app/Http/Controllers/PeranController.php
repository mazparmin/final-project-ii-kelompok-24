<?php

namespace App\Http\Controllers;

use App\Peran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PeranController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

 
    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'peran' => 'required',
            'film_id' => 'required',
            'cast_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $peran = Peran::create([
            'peran' => $request->peran,
            'film_id' => $request->film_id,
            'cast_id' => $request->cast_id,
        ]);

        if($peran){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Peran berhasil dibuat',
                'data'      =>  $peran
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Peran gagal dibuat'
        ], 409);
    }


    public function show($id)
    {
        $peran = Peran::find($id);

        if($peran)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Peran berhasil ditampilkan',
                'data'    => $peran
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Peran dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }


    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'peran' => 'required',
            'film_id' => 'required',
            'cast_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $peran = Peran::find($id);

        if($peran)
        {
             $peran->update([
                'peran' => $request->peran,
                'film_id' => $request->film_id,
                'cast_id' => $request->cast_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Peran dengan ID  : ' . $peran->id . '  berhasil diupdate',
                'data' =>    $peran
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Peran dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }


    public function destroy($id)
    {
        $peran = Peran::find($id);

        if ($peran) {
            $peran->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Peran berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Peran dengan id : ' .  $id . '  tidak ditemukan',
        ], 404); 
    }
    
}
