<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        $film = Film::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Film berhasil ditampilkan',
            'data'    => $film
        ]);
    }


    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $film = Film::create([
            'judul' =>  $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'poster' => $request->poster,
            'genre_id' => $request->genre_id
        ]);

        if($film){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Film berhasil dibuat',
                'data'      =>  $film
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Film gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        if($film)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Film berhasil ditampilkan',
                'data'    => $film
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Film dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $film = Film::find($id);

        if($film)
        {
             $film->update([
                'judul' =>  $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' => $request->poster,
                'genre_id' => $request->genre_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Film dengan Judul : ' . $film->judul . '  berhasil diupdate',
                'data' =>    $film
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Film dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        if ($film) {
            $film->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Film berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Film dengan id : ' .  $id . '  tidak ditemukan',
        ], 404); 
    }
}
