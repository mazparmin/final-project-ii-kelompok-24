<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{

protected $fillable = ['nama','umur', 'bio'];
protected $primaryKey = 'id';
protected $keyType = 'string';
public $incrementing ='false';

protected static function boot()
{
    parent::boot();

    static::creating(function($model){
        if (empty($model->{$model->getKeyName()})) {
            $model->{$model->getKeyName()} = Str::uuid();
        }
    });
}

public function perans()
{
    return $this->hasMany('App\Peran');
 
}

}