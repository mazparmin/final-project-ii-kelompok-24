<?php

use Illuminate\Http\Request;

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
});
*/

Route::apiResource('/cast', 'CastController');
Route::apiResource('/peran', 'PeranController');
Route::apiResource('/film', 'FilmController');
Route::apiResource('/genre', 'GenreController');
Route::apiResource('/user', 'UserController');
Route::apiResource('/profil', 'ProfilController');
Route::apiResource('/komentar', 'KomentarController');

Route::group([
        'prefix' => 'auth',
        'namespace' =>'Auth'
    ] , function() {
        Route::post('register', 'RegisterController')->name('auth.register');
        Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.egenerate-otp-code');
        Route::post('verification', 'VerificationController')->name('auth.verification');
        Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
        Route::post('login', 'LoginController')->name('auth.login');
    });